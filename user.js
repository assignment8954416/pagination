let results = [];

const loadMoreResults = async () => {
    const response = await fetch('https://randomuser.me/api/?results=20');
    const data = await response.json();
    return data.results;
};

const displayResults = () => {
    const resultsContainer = document.querySelector('#results-container');

    results.forEach((user) => {
        const userElement = document.createElement('div');
        userElement.classList.add('user');
        userElement.innerHTML = `
    <img src="${user.picture.large}" />
    <h2>${user.name.first} ${user.name.last}</h2>
    <p>${user.email}</p>
  `;
        resultsContainer.appendChild(userElement);
    });
};

const loadAndDisplayMoreResults = async () => {
    const spinnerContainer = document.querySelector('#spinner-container');

    spinnerContainer.style.display = 'block';

    const newResults = await loadMoreResults();
    results = [...results, ...newResults];
    displayResults();

    spinnerContainer.style.display = 'none';
};

loadAndDisplayMoreResults();

window.addEventListener('scroll', () => {

    const spinnerContainer = document.querySelector('#spinner-container');
    spinnerContainer.classList.add("move-spinner");
    let documentHeight = document.body.scrollHeight;
    let currentScroll = window.scrollY + window.innerHeight;
    // When the user is [modifier]px from the bottom, fire the event.
    let modifier = 5;

    if (currentScroll + modifier > documentHeight&& spinnerContainer.style.display === "none") {
        loadAndDisplayMoreResults();
    }
});